//
//  SettingVC.swift
//  ColorNote
//
//  Created by Nguyen van chien on 12/24/20.
//

import UIKit

class SettingVC: UIViewController {

    @IBOutlet weak var circleAutoBackup: UIView!
    @IBOutlet weak var viewDefaultColor: UIView!
    @IBOutlet weak var circleSyncOnLaunch: UIView!
    @IBOutlet weak var viewTop: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Radius viewTop
        viewTop.clipsToBounds = true
        viewTop.layer.cornerRadius = scale*30
        if #available(iOS 11.0, *) {
            viewTop.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        } else {
            // Fallback on earlier versions
        }
        
        //Circle view SyncOnLaunch
        circleSyncOnLaunch.layer.cornerRadius = scale*8
        circleSyncOnLaunch.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        circleSyncOnLaunch.layer.borderWidth = scale*1
        
        //Circle view Default Color
        viewDefaultColor.layer.cornerRadius = scale*15
        
        //Circle autoBackup
        circleAutoBackup.layer.cornerRadius = scale*8
        circleAutoBackup.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        circleAutoBackup.layer.borderWidth = scale*1
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    


}
