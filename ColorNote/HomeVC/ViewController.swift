//
//  ViewController.swift
//  ColorNote
//
//  Created by Nguyen van chien on 12/21/20.
//

import UIKit
var scale = UIScreen.main.bounds.height / 896

class ViewController: UIViewController {
    
    var ListTextNote = [ModelTextNote]()

    @IBOutlet weak var viewBgSearch: UIView!
    @IBOutlet weak var tab3: UIView!
    @IBOutlet weak var tab2: UIView!
    @IBOutlet weak var tab1: UIView!
    @IBOutlet weak var tab0: UIView!
    @IBOutlet weak var clvData: UICollectionView!
    
    override func viewWillAppear(_ animated: Bool) {
        tab0.isHidden = false
        tab1.isHidden = true
        tab2.isHidden = true
        tab3.isHidden = true
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        viewBgSearch.layer.cornerRadius = scale*25
        
    }

    
    @IBAction func btn3dot(_ sender: Any) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnAdd(_ sender: Any) {
        let alert = UIAlertController(title: "Add New Note", message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (ACTION) in
        }))
        
        let imageCheckList = UIImage(named: "iconCheckListAddBtn")
        let actionChecklist = UIAlertAction(title: "Checklist", style: .default, handler: {
            (ACTION) in
            let currenTime = Date()
            let dateCheckList = formaterDateCheckList(currenTime)
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddNoteVC") as! AddNoteVC
            vc.titleScreen = "Checklist"
            vc.type = 2
            vc.status = "addChecklist"
            TextNoteEntity.shared.insertTextNote(data: ModelTextNote(title: "Title", textNote: "", reminder: "", lock: "", archive: 0, delete: 0, type: 2, color: "",createTime: dateCheckList ,modifyTime: ""))
            UserDefaults.standard.set("add", forKey: "nextSc")
            self.navigationController?.pushViewController(vc, animated: true)
        })
        actionChecklist.setValue(imageCheckList, forKey: "image")
        alert.addAction(actionChecklist)
        
        let imageText = UIImage(named: "iconTextBtnAdd")
        let actionText = UIAlertAction(title: "Text", style: .default, handler: {
            (ACTION) in
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddNoteVC") as! AddNoteVC
            vc.titleScreen = "Text notes"
            vc.type = 1
            vc.status = "addTextnote"
            UserDefaults.standard.set("add", forKey: "nextSc")
            self.navigationController?.pushViewController(vc, animated: true)
        
        })
        actionText.setValue(imageText, forKey: "image")
        alert.addAction(actionText)
        
        if let popoverPresentationController = alert.popoverPresentationController {
            popoverPresentationController.sourceView = self.view
            popoverPresentationController.sourceRect = (sender as AnyObject).bounds
        }
        self.present(alert, animated: true, completion: nil)
                
    }
    @IBAction func btnTab0(_ sender: Any) {
        tab0.isHidden = false
        tab1.isHidden = true
        tab2.isHidden = true
        tab3.isHidden = true
    }
    @IBAction func btnTab1(_ sender: Any) {
        tab0.isHidden = true
        tab1.isHidden = false
        tab2.isHidden = true
        tab3.isHidden = true
    }
    @IBAction func btnTab2(_ sender: Any) {
        tab0.isHidden = true
        tab1.isHidden = true
        tab2.isHidden = false
        tab3.isHidden = true
    }
    @IBAction func btnTab3(_ sender: Any) {
        tab0.isHidden = true
        tab1.isHidden = true
        tab2.isHidden = true
        tab3.isHidden = false
    }

}



