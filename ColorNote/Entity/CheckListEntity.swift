//
//  CheckListEntity.swift
//  ColorNote
//
//  Created by Nguyễn Văn Chiến on 12/29/20.
//

import Foundation
import SQLite
class CheckListEntity{
    static let shared = CheckListEntity()
    
    private let dbCheckList = Table("DbCheckList")
    private let id = Expression<Int>("ID")
    private let status = Expression<Int>("Status")
    private let text = Expression<String>("Text")
    private let autoID = Expression<Int>("AutoID")

    
    func getData(idCheck: Int) -> [ModelCheckList]{
        var listData = [ModelCheckList]()
        do {
            if let listCate = try  Sqldata.shared.connection?.prepare(self.dbCheckList.filter(id == idCheck)) {
                for item in listCate {
                    listData.append(ModelCheckList(id: item[id], status: item[status], text: item[text], autoID: item[autoID]))
                }
            }
        } catch {
            print("Cannot get data from \(self.dbCheckList), Error is: \(error)")
        }
        return listData
    }
    
    
    func insertItemCheckList(data: ModelCheckList) -> Int64?
    {
        do{
            // Tạo câu truy vấn
            let querryInsert = dbCheckList.insert(self.id <- data.id,
                                                  self.text <- data.text,
                                                  self.status <- data.status)
            
            
            // kêt nối với database và thực hiện thêm dữ liệu
            let insertSuccess = try Sqldata.shared.connection?.run(querryInsert)
            
            // trả về ID của dòng đã được thêm
            print(insertSuccess!)
            return insertSuccess
            
        }catch
        {
            let nsError = error as NSError
            print("Cannot insert Deparment: Error is: \(nsError), \(nsError.userInfo)")
            return nil
        }
    }
    
    func updateText(idCheck:Int,textString:String) -> Int?
    {
        do{
            // Tạo câu truy vấn
            let querryInsert = dbCheckList.filter(autoID == idCheck).update(self.text <- textString)
            // kêt nối với database và thực hiện thêm dữ liệu
            let updateSuccess = try Sqldata.shared.connection?.run(querryInsert)
            
            // trả về ID của dòng đã được thêm
            print(updateSuccess!)
            return updateSuccess
            
        }catch
        {
            let nsError = error as NSError
            print("Cannot insert Deparment: Error is: \(nsError), \(nsError.userInfo)")
            return nil
        }
    }
    
    
    
    func updateStatus(idCheck:Int,stt: Int){
            if stt == 0 {
                do {
                    let updateSTT = dbCheckList.filter(autoID == idCheck)
                    if (try Sqldata.shared.connection?.run(updateSTT.update(status <- 1)))! > 0 {
                        print("updated alice")
                    } else {
                        print("alice not found")
                    }
                } catch {
                    print("update failed: \(error)")
                }
            } else {
                do {
                    let updateSTT = dbCheckList.filter(autoID == idCheck)
                    if (try Sqldata.shared.connection?.run(updateSTT.update(status <- 0)))! > 0 {
                        print("updated alice")
                    } else {
                        print("alice not found")
                    }
                } catch {
                    print("update failed: \(error)")
                }
            }
        }
    
    func deleteRow(idDeltete:Int){
        let alice = self.dbCheckList.filter(id == idDeltete)
        do {
            try  Sqldata.shared.connection?.run(alice.delete())
        } catch {
            print("Cannot get data from \(self.dbCheckList), Error is: \(error)")
        }
    }
}
