//
//  TextNoteEntity.swift
//  ColorNote
//
//  Created by Nguyễn Văn Chiến on 12/29/20.
//

import Foundation
import SQLite

class TextNoteEntity{
    static let shared = TextNoteEntity()
    
    private let dbColorNote = Table("DBColorNote")
    private let title = Expression<String>("Title")
    private let textNote = Expression<String?>("TextNote")
    private let reminder = Expression<String?>("Reminder")
    private let lock = Expression<String?>("Lock")
    private let archive = Expression<Int?>("Archive")
    private let delete = Expression<Int?>("Delete")
    private let id = Expression<Int>("ID")
    private let type = Expression<Int>("Type")
    private let color = Expression<String?>("Color")
    private let createTime = Expression<String?>("CreateTime")
    private let modifyTime = Expression<String?>("ModifyTime")
    
    func getData() -> [ModelTextNote]{
        var listData = [ModelTextNote]()
        do {
            if let listCate = try  Sqldata.shared.connection?.prepare(self.dbColorNote.filter(self.delete == 0).filter(self.archive == 0)) {
                for item in listCate {
                    listData.append(ModelTextNote(title: item[title], textNote: item[textNote] ?? "", reminder: item[reminder] ?? "", lock: item[lock] ?? "", archive: item[archive] ?? 0, delete: item[delete] ?? 0, id: item[id], type: item[type], color: item[color] ?? "#ffffff", createTime: item[createTime] ?? "", modifyTime: item[modifyTime] ?? ""))
                }
            }
        } catch {
            print("Cannot get data from \(self.dbColorNote), Error is: \(error)")
        }
        return listData
    }
    
    func getDataWhereColor(colorFilter: String) -> [ModelTextNote]{
        var listData = [ModelTextNote]()
        do {
            if let listCate = try  Sqldata.shared.connection?.prepare(self.dbColorNote.filter(self.color == colorFilter)) {
                for item in listCate {
                    listData.append(ModelTextNote(title: item[title], textNote: item[textNote] ?? "", reminder: item[reminder] ?? "", lock: item[lock] ?? "", archive: item[archive] ?? 0, delete: item[delete] ?? 0, id: item[id], type: item[type], color: item[color] ?? "#ffffff", createTime: item[createTime] ?? "", modifyTime: item[modifyTime] ?? ""))
                }
            }
        } catch {
            print("Cannot get data from \(self.dbColorNote), Error is: \(error)")
        }
        return listData
    }
    
    func sortByAlphabet() -> [ModelTextNote]{
        var listData = [ModelTextNote]()
        do {
            if let listCate = try  Sqldata.shared.connection?.prepare(self.dbColorNote.order(self.title)) {
                for item in listCate {
                    listData.append(ModelTextNote(title: item[title], textNote: item[textNote] ?? "", reminder: item[reminder] ?? "", lock: item[lock] ?? "", archive: item[archive] ?? 0, delete: item[delete] ?? 0, id: item[id], type: item[type], color: item[color] ?? "#ffffff", createTime: item[createTime] ?? "", modifyTime: item[modifyTime] ?? ""))
                }
            }
        } catch {
            print("Cannot get data from \(self.dbColorNote), Error is: \(error)")
        }
        return listData
    }
    func sortByColor() -> [ModelTextNote]{
        var listData = [ModelTextNote]()
        do {
            if let listCate = try  Sqldata.shared.connection?.prepare(self.dbColorNote.order(self.color)) {
                for item in listCate {
                    listData.append(ModelTextNote(title: item[title], textNote: item[textNote] ?? "", reminder: item[reminder] ?? "", lock: item[lock] ?? "", archive: item[archive] ?? 0, delete: item[delete] ?? 0, id: item[id], type: item[type], color: item[color] ?? "#ffffff", createTime: item[createTime] ?? "", modifyTime: item[modifyTime] ?? ""))
                }
            }
        } catch {
            print("Cannot get data from \(self.dbColorNote), Error is: \(error)")
        }
        return listData
    }
    func sortByCreateTime() -> [ModelTextNote]{
        var listData = [ModelTextNote]()
        do {
            if let listCate = try  Sqldata.shared.connection?.prepare(self.dbColorNote.order(self.createTime)) {
                for item in listCate {
                    listData.append(ModelTextNote(title: item[title], textNote: item[textNote] ?? "", reminder: item[reminder] ?? "", lock: item[lock] ?? "", archive: item[archive] ?? 0, delete: item[delete] ?? 0, id: item[id], type: item[type], color: item[color] ?? "#ffffff", createTime: item[createTime] ?? "", modifyTime: item[modifyTime] ?? ""))
                }
            }
        } catch {
            print("Cannot get data from \(self.dbColorNote), Error is: \(error)")
        }
        return listData
    }
    func sortByModifyTime() -> [ModelTextNote]{
        var listData = [ModelTextNote]()
        do {
            if let listCate = try  Sqldata.shared.connection?.prepare(self.dbColorNote.order(self.modifyTime)) {
                for item in listCate {
                    listData.append(ModelTextNote(title: item[title], textNote: item[textNote] ?? "", reminder: item[reminder] ?? "", lock: item[lock] ?? "", archive: item[archive] ?? 0, delete: item[delete] ?? 0, id: item[id], type: item[type], color: item[color] ?? "#ffffff", createTime: item[createTime] ?? "", modifyTime: item[modifyTime] ?? ""))
                }
            }
        } catch {
            print("Cannot get data from \(self.dbColorNote), Error is: \(error)")
        }
        return listData
    }
    
    
    
    func insertTextNote(data: ModelTextNote) -> Int64?
    {
        do{
            // Tạo câu truy vấn
            let querryInsert = dbColorNote.insert(self.title <- data.title,
                                                  self.textNote <- data.textNote,
                                                  self.reminder <- data.reminder,
                                                  self.lock <- data.lock,
                                                  self.archive <- data.archive,
                                                  self.delete <- data.delete,
                                                  self.type <- data.type,
                                                  self.color <- data.color,
                                                  self.createTime <- data.createTime,
                                                  self.modifyTime <- data.modifyTime)
            
            
            // kêt nối với database và thực hiện thêm dữ liệu
            let insertSuccess = try Sqldata.shared.connection?.run(querryInsert)
            
            // trả về ID của dòng đã được thêm
            print(insertSuccess!)
            return insertSuccess
            
        }catch
        {
            let nsError = error as NSError
            print("Cannot insert Deparment: Error is: \(nsError), \(nsError.userInfo)")
            return nil
        }
    }
    
    func deleteRow(idDeltete:Int){
        let alice = self.dbColorNote.filter(id == idDeltete)
        do {
            try  Sqldata.shared.connection?.run(alice.delete())
        } catch {
            print("Cannot get data from \(self.dbColorNote), Error is: \(error)")
        }
    }
    
    func getDataColor(idCheck: Int) -> String{
        do {
            if let list = try Sqldata.shared.connection?.prepare(self.dbColorNote.filter(id == idCheck)){
                for item in list {
                    return item[color] ?? ""
                }
            }
            return ""
        } catch {
            print("Cannot get data from \(self.dbColorNote), Error is: \(error)")
            return ""
        }
    }
    
    func getDataTitle(idCheck: Int) -> String{
        do {
            if let list = try Sqldata.shared.connection?.prepare(self.dbColorNote.filter(id == idCheck)){
                for item in list {
                    return item[title] 
                }
            }
            return ""
        } catch {
            print("Cannot get data from \(self.dbColorNote), Error is: \(error)")
            return ""
        }
    }
    
    func getTextNote(idCheck: Int) -> String{
        do {
            if let list = try Sqldata.shared.connection?.prepare(self.dbColorNote.filter(id == idCheck)){
                for item in list {
                    return item[textNote] ?? ""
                }
            }
            return ""
        } catch {
            print("Cannot get data from \(self.dbColorNote), Error is: \(error)")
            return ""
        }
    }
    
    func updateColorTitleTextNote(idCheck: Int, colorUpdate: String, titleUpdate: String, textNoteUpdate: String) -> Int?{
        do{
            // Tạo câu truy vấn
            let querryInsert = dbColorNote.filter(id == idCheck).update(self.color <- colorUpdate, self.title <- titleUpdate, self.textNote <- textNoteUpdate)
            // kêt nối với database và thực hiện thêm dữ liệu
            let updateSuccess = try Sqldata.shared.connection?.run(querryInsert)
            
            // trả về ID của dòng đã được thêm
            print(updateSuccess!)
            return updateSuccess
        }catch
        {
            let nsError = error as NSError
            print("Cannot insert Deparment: Error is: \(nsError), \(nsError.userInfo)")
            return nil
        }

    }
    
    func updateModifiTime(idCheck: Int, updateModifyTime: String) -> Int?{
        do{
            // Tạo câu truy vấn
            let querryInsert = dbColorNote.filter(id == idCheck).update(self.modifyTime <- updateModifyTime)
            // kêt nối với database và thực hiện thêm dữ liệu
            let updateSuccess = try Sqldata.shared.connection?.run(querryInsert)
            
            // trả về ID của dòng đã được thêm
            print(updateSuccess!)
            return updateSuccess
        }catch
        {
            let nsError = error as NSError
            print("Cannot insert Deparment: Error is: \(nsError), \(nsError.userInfo)")
            return nil
        }
    }
    
    func updateCreateTime(idCheck: Int, updateCreateTime: String) -> Int?{
        do{
            // Tạo câu truy vấn
            let querryInsert = dbColorNote.filter(id == idCheck).update(self.createTime <- updateCreateTime)
            // kêt nối với database và thực hiện thêm dữ liệu
            let updateSuccess = try Sqldata.shared.connection?.run(querryInsert)
            
            // trả về ID của dòng đã được thêm
            print(updateSuccess!)
            return updateSuccess
        }catch
        {
            let nsError = error as NSError
            print("Cannot insert Deparment: Error is: \(nsError), \(nsError.userInfo)")
            return nil
        }
    }
    
    func updateArchive(idCheck:Int, archiveUpdate:Int ){
           if archiveUpdate == 0 {
               do {
                   let update = dbColorNote.filter(id == idCheck)
                   if (try Sqldata.shared.connection?.run(update.update(archive <- 1)))! > 0 {
                       print("updated alice")
                   } else {
                       print("alice not found")
                   }
               } catch {
                   print("update failed: \(error)")
               }
           } else {
               do {
                   let update = dbColorNote.filter(id == idCheck)
                   if (try Sqldata.shared.connection?.run(update.update(archive <- 0)))! > 0 {
                       print("updated alice")
                   } else {
                       print("alice not found")
                   }
               } catch {
                   print("update failed: \(error)")
               }
           }
       }

    func updateDelete(idCheck:Int, deleteUpdate:Int ){
           if deleteUpdate == 0 {
               do {
                   let update = dbColorNote.filter(id == idCheck)
                   if (try Sqldata.shared.connection?.run(update.update(delete <- 1)))! > 0 {
                       print("updated alice")
                   } else {
                       print("alice not found")
                   }
               } catch {
                   print("update failed: \(error)")
               }
           } else {
               do {
                   let update = dbColorNote.filter(id == idCheck)
                   if (try Sqldata.shared.connection?.run(update.update(delete <- 0)))! > 0 {
                       print("updated alice")
                   } else {
                       print("alice not found")
                   }
               } catch {
                   print("update failed: \(error)")
               }
           }
       }
    
    func getArchive() -> [ModelTextNote]{
        var listData = [ModelTextNote]()
        do {
            if let listCate = try  Sqldata.shared.connection?.prepare(self.dbColorNote.filter(self.archive == 1)) {
                for item in listCate {
                    listData.append(ModelTextNote(title: item[title], textNote: item[textNote] ?? "", reminder: item[reminder] ?? "", lock: item[lock] ?? "", archive: item[archive] ?? 0, delete: item[delete] ?? 0, id: item[id], type: item[type], color: item[color] ?? "#ffffff", createTime: item[createTime] ?? "", modifyTime: item[modifyTime] ?? ""))
                }
            }
        } catch {
            print("Cannot get data from \(self.dbColorNote), Error is: \(error)")
        }
        return listData
    }
    
    func getDataDelete() -> [ModelTextNote]{
        var listData = [ModelTextNote]()
        do {
            if let listCate = try  Sqldata.shared.connection?.prepare(self.dbColorNote.filter(self.delete == 1)) {
                for item in listCate {
                    listData.append(ModelTextNote(title: item[title], textNote: item[textNote] ?? "", reminder: item[reminder] ?? "", lock: item[lock] ?? "", archive: item[archive] ?? 0, delete: item[delete] ?? 0, id: item[id], type: item[type], color: item[color] ?? "#ffffff", createTime: item[createTime] ?? "", modifyTime: item[modifyTime] ?? ""))
                }
            }
        } catch {
            print("Cannot get data from \(self.dbColorNote), Error is: \(error)")
        }
        return listData
    }
    
    func getStatusDelete(idCheck: Int) -> Int{
        do {
            if let list = try Sqldata.shared.connection?.prepare(self.dbColorNote.filter(id == idCheck)){
                for item in list {
                    return item[delete] ?? 0
                }
            }
            return 0
        } catch {
            print("Cannot get data from \(self.dbColorNote), Error is: \(error)")
            return 0
        }
    }
    
    func getStatusArchive(idCheck: Int) -> Int{
        do {
            if let list = try Sqldata.shared.connection?.prepare(self.dbColorNote.filter(id == idCheck)){
                for item in list {
                    return item[archive] ?? 0
                }
            }
            return 0
        } catch {
            print("Cannot get data from \(self.dbColorNote), Error is: \(error)")
            return 0
        }
    }
    
    func getType(idCheck: Int) -> Int{
        do {
            if let list = try Sqldata.shared.connection?.prepare(self.dbColorNote.filter(id == idCheck)){
                for item in list {
                    return item[type]
                }
            }
            return 0
        } catch {
            print("Cannot get data from \(self.dbColorNote), Error is: \(error)")
            return 0
        }
    }
    
}
