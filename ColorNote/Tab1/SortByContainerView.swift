//
//  SortByContainerView.swift
//  ColorNote
//
//  Created by Nguyen van chien on 12/25/20.
//

import UIKit

class SortByContainerView: UIViewController {

    @IBOutlet weak var byReminder: UIView!
    @IBOutlet weak var byColor: UIView!
    @IBOutlet weak var byAlphabet: UIView!
    @IBOutlet weak var byCreateTime: UIView!
    @IBOutlet weak var byModifiedTime: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        byReminder.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapReminder(_:))))
        byColor.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapColor(_:))))
        byAlphabet.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapAlphabet(_:))))
        byCreateTime.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapCreateTime(_:))))
        byModifiedTime.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapModifiedTime(_:))))
    }
    @objc func tapReminder(_ sender: UITapGestureRecognizer) {
    
    }
    @objc func tapColor(_ sender: UITapGestureRecognizer){
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadSortByColor"), object: nil)
    }
    @objc func tapAlphabet(_ sender: UITapGestureRecognizer){
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadSortByAlphabet"), object: nil)
    }
    @objc func tapCreateTime(_ sender: UITapGestureRecognizer){
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadSortByCreateTime"), object: nil)
    }
    @objc func tapModifiedTime(_ sender: UITapGestureRecognizer){
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadSortByModifyTime"), object: nil)
    }
    
    @IBAction func byModifiedTime(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadSortByModifyTime"), object: nil)

    }
    @IBAction func byCreateTime(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadSortByCreateTime"), object: nil)

    }
    @IBAction func byAlphabet(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadSortByAlphabet"), object: nil)
    }
    @IBAction func byColor(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadSortByColor"), object: nil)
    }
    @IBAction func byReminderTime(_ sender: Any) {
    }
}
