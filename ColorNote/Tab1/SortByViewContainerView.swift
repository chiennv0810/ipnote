//
//  SortByViewContainerView.swift
//  ColorNote
//
//  Created by Apple on 12/29/20.
//

import UIKit

class SortByViewContainerView: UIViewController {

    @IBOutlet weak var largeGrid: UIView!
    @IBOutlet weak var grid: UIView!
    @IBOutlet weak var detail: UIView!
    @IBOutlet weak var list: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        list.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapList(_:))))
        detail.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapDetail(_:))))
        grid.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapGrid(_:))))
        largeGrid.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapLargeGrid(_:))))
    }
    @objc func tapList(_ sender: UITapGestureRecognizer) {
        UserDefaults.standard.set("list", forKey: "sortByView")
        pushNotification()
    }
    @objc func tapDetail(_ sender: UITapGestureRecognizer) {
        UserDefaults.standard.set("detail", forKey: "sortByView")
        pushNotification()
    }
    @objc func tapGrid(_ sender: UITapGestureRecognizer) {
        UserDefaults.standard.set("grid", forKey: "sortByView")
        pushNotification()
    }
    @objc func tapLargeGrid(_ sender: UITapGestureRecognizer) {
        UserDefaults.standard.set("largeGrid", forKey: "sortByView")
        pushNotification()
    }
    
    @IBAction func list(_ sender: Any) {
        UserDefaults.standard.set("list", forKey: "sortByView")
        pushNotification()
    }
    @IBAction func detail(_ sender: Any) {
        UserDefaults.standard.set("detail", forKey: "sortByView")
        pushNotification()
    }
    @IBAction func grid(_ sender: Any) {
        UserDefaults.standard.set("grid", forKey: "sortByView")
        pushNotification()
    }
    @IBAction func largeGrid(_ sender: Any) {
        UserDefaults.standard.set("largeGrid", forKey: "sortByView")
        pushNotification()
    }
    
    func pushNotification(){
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadSortByView"), object: nil)
    }
}
