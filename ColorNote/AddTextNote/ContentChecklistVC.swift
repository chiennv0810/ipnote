//
//  ContentChecklistVC.swift
//  ColorNote
//
//  Created by Nguyen van chien on 12/24/20.
//

import UIKit

class ContentChecklistVC: UIViewController {
    var listDataTextNote = [ModelTextNote]()
    var listDataCheckList = [ModelCheckList]()
    let status = UserDefaults.standard.string(forKey: "nextSc")
    var indexSelect = -1
    @IBOutlet weak var clvCheckList: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        clvCheckList.backgroundColor = .clear
        clvCheckList.delegate = self
        clvCheckList.dataSource = self
        clvCheckList.backgroundColor = .clear
        clvCheckList.register(UINib(nibName: "CellDataCheckList", bundle: nil), forCellWithReuseIdentifier: "CellDataCheckList")
        
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: clvCheckList.bounds.width, height: scale*20)
        layout.minimumLineSpacing = scale * 16
        layout.sectionInset.top = scale * 28
        layout.sectionInset.bottom = scale * 16
        clvCheckList.collectionViewLayout = layout
        
        let status = UserDefaults.standard.string(forKey: "nextSc")
        if status! == "nextSc" {
            listDataCheckList = CheckListEntity.shared.getData(idCheck: AddNoteVC.shared.id)
        }
    }
}

extension ContentChecklistVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listDataCheckList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = clvCheckList.dequeueReusableCell(withReuseIdentifier: "CellDataCheckList", for: indexPath) as! CellDataCheckList
        cell.indexPath = indexPath
        
        
        let status = UserDefaults.standard.string(forKey: "nextSc")
        if status! == "nextSc" {
            cell.tfName.text = listDataCheckList[indexPath.row].text
            let color = TextNoteEntity.shared.getDataColor(idCheck: AddNoteVC.shared.id)
            if color == ColorDefine.colorWhite {
                cell.tfName.textColor = .black
            } else {
                cell.tfName.textColor = .white
            }
            if listDataCheckList[indexPath.row].status == 0 {
                let color = TextNoteEntity.shared.getDataColor(idCheck: AddNoteVC.shared.id)
                if color == ColorDefine.colorWhite {
                    cell.imgCircle.image = UIImage(named: "circleBlack")
                } else {
                    cell.imgCircle.image = UIImage(named: "circleWhite")
                }
            } else {
                let color = TextNoteEntity.shared.getDataColor(idCheck: AddNoteVC.shared.id)
                if color == ColorDefine.colorWhite {
                    cell.imgCircle.image = UIImage(named: "circleDoneBlue")
                    let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: listDataCheckList[indexPath.row].text)
                        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
                    cell.tfName.attributedText = attributeString
                } else {
                    cell.imgCircle.image = UIImage(named: "circleDoneWhite")
                    let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: listDataCheckList[indexPath.row].text)
                        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
                    cell.tfName.attributedText = attributeString
                }
            }
        } else {
            cell.tfName.text = listDataCheckList[indexPath.row].text
            cell.tfName.textColor = .black
            if listDataCheckList[indexPath.row].status == 0 {
                cell.imgCircle.image = UIImage(named: "circleBlack")
            } else {
                cell.imgCircle.image = UIImage(named: "circleDoneBlue")
                let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: listDataCheckList[indexPath.row].text)
                    attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
                cell.tfName.attributedText = attributeString
            }
        }
        cell.tfName.addTarget(self, action: #selector(ContentChecklistVC.textFieldDidChange(_:)), for: .editingChanged)
        return cell
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if let view = textField.superview{
            if let cell = view.superview as? CellDataCheckList{
                if textField.text != "" {
                    let que = CheckListEntity.shared.updateText(idCheck: listDataCheckList[cell.indexPath.row].autoID, textString: textField.text ?? "")
                    print(que!)
                }
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notifiUpdateModifyTime"), object: nil)
            }
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: scale*20)
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if let sectionFooter = clvCheckList.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: "AddFooterCheckList", for: indexPath) as? AddFooterCheckList{
            sectionFooter.lblAdd.textColor = UIColor(hexString: "#3369ff")
            let color = TextNoteEntity.shared.getDataColor(idCheck: AddNoteVC.shared.id)
            let status = UserDefaults.standard.string(forKey: "nextSc")
            if status! == "nextSc" {
                if color == ColorDefine.colorWhite {
                    sectionFooter.lblAdd.textColor = UIColor(hexString: "#3369ff")
                    sectionFooter.iconAdd.image = UIImage(named: "icAddCheckListBlue")
                } else {
                    sectionFooter.lblAdd.textColor = UIColor(hexString: "#ffffff")
                    sectionFooter.iconAdd.image = UIImage(named: "icAddCheckListWhite")
                }
            }
            sectionFooter.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(clickFooter(_:))))
            return sectionFooter
        }
        return UICollectionReusableView()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        indexSelect = indexPath.row
        if indexSelect == indexPath.row {
            CheckListEntity.shared.updateStatus(idCheck: listDataCheckList[indexPath.row].autoID, stt: listDataCheckList[indexPath.row].status)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notifiUpdateModifyTime"), object: nil)
        }
        listDataTextNote = TextNoteEntity.shared.getData()
        if status == "nextSc" {
            listDataCheckList = CheckListEntity.shared.getData(idCheck: AddNoteVC.shared.id)
        } else {
            listDataCheckList = CheckListEntity.shared.getData(idCheck: listDataTextNote[listDataTextNote.count-1].id)
        }
        
        clvCheckList.reloadData()
    }
    
    @objc func clickFooter(_ sender: UITapGestureRecognizer){
        listDataTextNote = TextNoteEntity.shared.getData()
        if status == "nextSc" {
            let que = CheckListEntity.shared.insertItemCheckList(data: ModelCheckList(id: AddNoteVC.shared.id, status: 0, text: "Name"))
            listDataCheckList = CheckListEntity.shared.getData(idCheck: AddNoteVC.shared.id)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notifiUpdateModifyTime"), object: nil)
            print(que!)
        } else {
            let que = CheckListEntity.shared.insertItemCheckList(data: ModelCheckList(id: listDataTextNote[listDataTextNote.count-1].id, status: 0, text: "Name"))
            listDataCheckList = CheckListEntity.shared.getData(idCheck: listDataTextNote[listDataTextNote.count-1].id)
            print(que!)
        }
        clvCheckList.reloadData()
    }
}
