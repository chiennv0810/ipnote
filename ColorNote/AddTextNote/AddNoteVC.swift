//
//  AddNoteVC.swift
//  ColorNote
//
//  Created by Nguyen van chien on 12/24/20.
//

import UIKit



class AddNoteVC: UIViewController {
    static var shared:AddNoteVC = AddNoteVC()
    var indexSelect = 0
    var titleScreen:String = ""
    var type:Int = 0
    var id:Int = 0{
        didSet{
            textNote = TextNoteEntity.shared.getTextNote(idCheck: id)
        }
    }
    var status:String = ""
    
    var textNote = ""
    var dateTextNote = ""
    var dateCheckList = ""
    let color:[String] = [ColorDefine.colorWhite,ColorDefine.colorYellow,ColorDefine.colorGreen,ColorDefine.colorPurple,ColorDefine.colorGrey,ColorDefine.colorRed,ColorDefine.colorOrange,ColorDefine.colorBlue,ColorDefine.colorMoss,ColorDefine.colorBlack]
    
    var listDataTextNote = [ModelTextNote]()
    var listDataCheckList = [ModelCheckList]()
    
    @IBOutlet weak var btnDelete: UIView!
    @IBOutlet weak var btnArchive: UIView!
    @IBOutlet weak var btnLock: UIView!
    @IBOutlet weak var btnShare: UIView!
    @IBOutlet weak var btnReminder: UIView!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnPrevious: UIButton!
    @IBOutlet weak var btn3dot: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet var viewBgColor: UIView!
    @IBOutlet weak var titleContent: UITextField!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var bgViewBottom: UIView!
    @IBOutlet weak var contentChecklist: UIView!
    @IBOutlet weak var contentTextNote: UIView!
    @IBOutlet weak var clvSelectColor: UICollectionView!
    @IBOutlet weak var lblTitle: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(textViewChange(_:)), name: NSNotification.Name(rawValue: "contentTextNote"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateModifyTime(_:)), name: NSNotification.Name(rawValue: "notifiUpdateModifyTime"), object: nil)
        
        
        //Setup collectionview Select color
        clvSelectColor.backgroundColor = .clear
        clvSelectColor.dataSource = self
        clvSelectColor.delegate = self
        clvSelectColor.register(UINib(nibName: "CellSelectColor", bundle: nil), forCellWithReuseIdentifier: "CellSelectColor")
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.itemSize = CGSize(width: scale*40, height: scale*40)
        layout.sectionInset.right = scale*25
        layout.sectionInset.left = scale*25
        layout.minimumLineSpacing = scale*15
        clvSelectColor.collectionViewLayout = layout
        
        //Setup title Screen
        lblTitle.text = titleScreen
        
        bgViewBottom.clipsToBounds = true
        bgViewBottom.layer.cornerRadius = scale*30
        if #available(iOS 11.0, *) {
            bgViewBottom.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        } else {
            // Fallback on earlier versions
        }
        
        if status == "nextSc" {
            titleContent.text = TextNoteEntity.shared.getDataTitle(idCheck: id)
            
        }
        
        setIndexSelect()
        
        if id != 0 {
            viewBgColor.backgroundColor = UIColor(hexString: TextNoteEntity.shared.getDataColor(idCheck: id))
        } else {
            viewBgColor.backgroundColor = UIColor(hexString: "#ffffff")
        }
        
        if id != 0 {
            let color = TextNoteEntity.shared.getDataColor(idCheck: id)
            if color != ColorDefine.colorWhite{
                btnBack.setImage(UIImage(named: "icBackWhite"), for: .normal)
                lblTitle.textColor = .white
                titleContent.textColor = .white
                btnPrevious.setImage(UIImage(named: "btnPreviousWhite"), for: .normal)
                btnNext.setImage(UIImage(named: "btnNextWhite"), for: .normal)
            } else {
                btnBack.setImage(UIImage(named: "icArrLeftCircle"), for: .normal)
                lblTitle.textColor = .black
                titleContent.textColor = .black
                btnPrevious.setImage(UIImage(named: "btnPrevious"), for: .normal)
                btnNext.setImage(UIImage(named: "btnNext"), for: .normal)
            }
        }
        btnReminder.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(clickReminder(_:))))
        btnShare.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(clickShare(_:))))
        btnLock.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(clickLock(_:))))
        btnArchive.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(clickArchive(_:))))
        btnDelete.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(clickDelete(_:))))
        

    }
    @objc func clickReminder(_ sender: UITapGestureRecognizer){
        
    }
    @objc func clickShare(_ sender: UITapGestureRecognizer){
        
    }
    @objc func clickLock(_ sender: UITapGestureRecognizer){
        
    }
    @objc func clickArchive(_ sender: UITapGestureRecognizer){
        
    }
    @objc func clickDelete(_ sender: UITapGestureRecognizer){
        if id != 0 {
            let alert = UIAlertController(title: "Do you want delete this " + titleScreen, message: "After deleting, this " + titleScreen + " will be moved to the trash", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "No", style: .default, handler: { action in
                print("No")
            }))
            alert.addAction(UIAlertAction(title: "Yes", style: .cancel, handler: { action in
                TextNoteEntity.shared.updateDelete(idCheck: self.id, deleteUpdate: TextNoteEntity.shared.getStatusDelete(idCheck: self.id))
                self.navigationController?.popViewController(animated: true)
                self.navigationController?.view.makeToast("This " + self.titleScreen + " has been move to trash")
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadData"), object: nil)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadDataDelete"), object: nil)
            }))
            self.present(alert, animated: true)
        }
        
    }
    @objc func textViewChange(_ sender: Notification){
        textNote = sender.userInfo?["text"] as! String
    }
    @objc func updateModifyTime(_ sender: Notification){
        let updateModifyTime = TextNoteEntity.shared.updateModifiTime(idCheck: id, updateModifyTime: dateCheckList)
        print(updateModifyTime!)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        getDateTime()
        if titleScreen == "Text notes" {
            contentTextNote.isHidden = false
            contentChecklist.isHidden = true
            type = 1
        } else {
            contentChecklist.isHidden = false
            contentTextNote.isHidden = true
            type = 2
        }
    }
    
    func getDateTime(){
        let currenTime = Date()
        let dateTimeString = formaterDate(currenTime)
        dateTextNote = formaterDateNote(currenTime)
        dateCheckList = formaterDateCheckList(currenTime)
        lblDate.text = dateTimeString
        lblDate.textColor = UIColor(rgb: 0xFFFFFF).withAlphaComponent(0.6)
    }
    
    
    
    @IBAction func btnBack(_ sender: Any) {
        if type == 1 {
            if id != 0 {
                let que = TextNoteEntity.shared.updateColorTitleTextNote(idCheck: id, colorUpdate: color[indexSelect], titleUpdate: titleContent.text ?? "Title", textNoteUpdate: textNote)
                
                print(que!)
            } else {
                if textNote != "" {
                    let que = TextNoteEntity.shared.insertTextNote(data: ModelTextNote(title: titleContent.text ?? "Title", textNote: textNote, reminder: dateTextNote , lock: "", archive: 0, delete: 0, type: 1, color: color[indexSelect], createTime: dateTextNote ,modifyTime: ""))
                
                    print(que!)
                }
            }
        } else {
            listDataTextNote = TextNoteEntity.shared.getData()
            if id != 0{
                listDataCheckList = CheckListEntity.shared.getData(idCheck: id)
                if listDataCheckList.count == 0 {
                    TextNoteEntity.shared.deleteRow(idDeltete: id)
                } else {
                    let que = TextNoteEntity.shared.updateColorTitleTextNote(idCheck: id, colorUpdate: color[indexSelect], titleUpdate: titleContent.text ?? "Title", textNoteUpdate: "")
                    
                    print(que!)
                }
            } else {
                listDataCheckList = CheckListEntity.shared.getData(idCheck: listDataTextNote[listDataTextNote.count-1].id)
                if listDataCheckList.count == 0 {
                    TextNoteEntity.shared.deleteRow(idDeltete: listDataTextNote[listDataTextNote.count-1].id)
                } else {
                    let que = TextNoteEntity.shared.updateColorTitleTextNote(idCheck: listDataTextNote[listDataTextNote.count-1].id, colorUpdate: color[indexSelect], titleUpdate: titleContent.text ?? "Title", textNoteUpdate: "")
                    let updateCreateTime = TextNoteEntity.shared.updateCreateTime(idCheck: listDataTextNote[listDataTextNote.count-1].id, updateCreateTime: dateCheckList)
                    print(updateCreateTime!)
                    print(que!)
                    
                }
            }
           
        }
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadData"), object: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn3dot(_ sender: Any) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnPrevious(_ sender: Any) {
    }
    @IBAction func btnNext(_ sender: Any) {
    }
}

extension AddNoteVC:UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return color.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = clvSelectColor.dequeueReusableCell(withReuseIdentifier: "CellSelectColor", for: indexPath) as! CellSelectColor
        cell.colorCircle.backgroundColor = UIColor(hexString: color[indexPath.row])
        
        
        if indexSelect == indexPath.row {
            cell.icSelected.isHidden = false
        } else {
            cell.icSelected.isHidden = true
        }
        
        if indexSelect == 0{
            cell.icSelected.image = UIImage(named: "icV")
        } else {
            cell.icSelected.image = UIImage(named: "icDoneWhite")
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.indexSelect = indexPath.row
        self.clvSelectColor.reloadData()
    }
    
    func setIndexSelect(){
        if id != 0 {
            let getColor = TextNoteEntity.shared.getDataColor(idCheck: id)
            switch getColor {
            case ColorDefine.colorWhite :
                indexSelect = 0
            case ColorDefine.colorYellow :
                indexSelect = 1
            case ColorDefine.colorGreen :
                indexSelect = 2
            case ColorDefine.colorPurple :
                indexSelect = 3
            case ColorDefine.colorGrey :
                indexSelect = 4
            case ColorDefine.colorRed :
                indexSelect = 5
            case ColorDefine.colorOrange :
                indexSelect = 6
            case ColorDefine.colorBlue :
                indexSelect = 7
            case ColorDefine.colorMoss :
                indexSelect = 8
            case ColorDefine.colorBlack :
                indexSelect = 9
            default:
                indexSelect = 0
            }
        }
        
    }
}


