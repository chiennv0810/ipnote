//
//  CellSelectColor.swift
//  ColorNote
//
//  Created by Nguyen van chien on 12/24/20.
//

import UIKit

class CellSelectColor: UICollectionViewCell {
    let scale = UIScreen.main.bounds.height / 896

    @IBOutlet weak var icSelected: UIImageView!
    @IBOutlet weak var colorCircle: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        colorCircle.layer.cornerRadius = scale * 20
        colorCircle.clipsToBounds = true
        
    }
    
    
}
