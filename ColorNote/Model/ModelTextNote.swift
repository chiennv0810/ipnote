//
//  ModelTextNote.swift
//  ColorNote
//
//  Created by Nguyễn Văn Chiến on 12/29/20.
//

import Foundation
class ModelTextNote{
    var title:String = ""
    var textNote:String = ""
    var reminder:String = ""
    var lock:String = ""
    var archive:Int = 0
    var delete:Int = 0
    var id:Int = 0
    var type:Int = 0
    var color:String = ""
    var createTime:String = ""
    var modifyTime:String = ""
    
    init(title: String, textNote: String, reminder: String, lock: String, archive: Int, delete: Int, id: Int, type: Int, color: String, createTime: String, modifyTime: String) {
        self.title = title
        self.textNote = textNote
        self.reminder = reminder
        self.lock = lock
        self.archive = archive
        self.delete = delete
        self.id = id
        self.type = type
        self.color = color
        self.createTime = createTime
        self.modifyTime = modifyTime
    }
    
    init(title: String, textNote: String, reminder: String, lock: String, archive: Int, delete: Int, type: Int, color: String, createTime: String, modifyTime: String) {
        self.title = title
        self.textNote = textNote
        self.reminder = reminder
        self.lock = lock
        self.archive = archive
        self.delete = delete
        self.type = type
        self.color = color
        self.createTime = createTime
        self.modifyTime = modifyTime
    }
}
