//
//  ModelCheckList.swift
//  ColorNote
//
//  Created by Nguyễn Văn Chiến on 12/29/20.
//

import Foundation
class ModelCheckList{
    var id:Int = 0
    var status:Int = 0
    var text:String = ""
    var autoID:Int = 0
    
    init(id: Int, status: Int, text: String, autoID: Int) {
        self.id = id
        self.status = status
        self.text = text
        self.autoID = autoID
    }
    
    init(id: Int, status: Int, text: String) {
        self.id = id
        self.status = status
        self.text = text
    }
}
