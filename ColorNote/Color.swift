//
//  Color.swift
//  ColorNote
//
//  Created by Nguyễn Văn Chiến on 1/11/21.
//

import Foundation
struct ColorDefine {
    static var colorWhite = "#ffffff"
    static var colorYellow = "#FEB62D"
    static var colorGreen = "#12C696"
    static var colorPurple = "#C812CC"
    static var colorGrey = "#999999"
    static var colorRed = "#E8505B"
    static var colorOrange = "#FF7746"
    static var colorBlue = "#3369FF"
    static var colorMoss = "#263328"
    static var colorBlack = "#171C26"
}
