//
//  CellCheckList.swift
//  ColorNote
//
//  Created by Nguyễn Văn Chiến on 12/30/20.
//

import UIKit

class CellCheckList: UICollectionViewCell {
    
    var listDataCheckList = [ModelCheckList]()
    var listDataColor = [ModelTextNote]()
    
    var color = ""

    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var borderChecklistButton: UIView!
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var checklistBottom: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var clvData: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        viewBg.layer.cornerRadius = scale * 16
        borderChecklistButton.layer.cornerRadius = scale * 5
        borderChecklistButton.layer.borderWidth = scale * 1
        borderChecklistButton.layer.borderColor = UIColor(rgb: 0xffffff).withAlphaComponent(0.6).cgColor
        
        // Initialization code
        clvData.delegate = self
        clvData.dataSource = self
        clvData.backgroundColor = .clear
        clvData.register(UINib(nibName: "CellDataCheckList", bundle: nil), forCellWithReuseIdentifier: "CellDataCheckList")
        
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: scale*143, height: scale*20)
        layout.minimumLineSpacing = scale*11
        clvData.collectionViewLayout = layout
    }
    
    func setup(id:Int) {
        listDataCheckList = CheckListEntity.shared.getData(idCheck: id)
        color = TextNoteEntity.shared.getDataColor(idCheck: id)
        clvData.reloadData()
    }

}

extension CellCheckList: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
         return listDataCheckList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = clvData.dequeueReusableCell(withReuseIdentifier: "CellDataCheckList", for: indexPath) as! CellDataCheckList
        cell.tfName.text = listDataCheckList[indexPath.row].text
        cell.tfName.isUserInteractionEnabled = false

        
        if color == "#ffffff" {
            cell.tfName.textColor = .black
            borderChecklistButton.layer.borderColor = UIColor(rgb: 0x000000).withAlphaComponent(0.6).cgColor
            checklistBottom.textColor = UIColor(rgb: 0x000000).withAlphaComponent(0.6)
            if listDataCheckList[indexPath.row].status == 1 {
                cell.imgCircle.image = UIImage(named: "circleDoneBlue")
                let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: listDataCheckList[indexPath.row].text)
                    attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
                cell.tfName.attributedText = attributeString
            } else {
                cell.imgCircle.image = UIImage(named: "circleBlack")

            }
        } else {
            cell.tfName.textColor = .white
            borderChecklistButton.layer.borderColor = UIColor(rgb: 0xffffff).withAlphaComponent(0.6).cgColor

            if listDataCheckList[indexPath.row].status == 1 {
                cell.imgCircle.image = UIImage(named: "circleDoneWhite")
                let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: listDataCheckList[indexPath.row].text)
                    attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
                cell.tfName.attributedText = attributeString
            } else {
                cell.imgCircle.image = UIImage(named: "circleWhite")
            }
        }
        
        return cell
    }
}
