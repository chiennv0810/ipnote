//
//  CellGrid.swift
//  ColorNote
//
//  Created by Nguyễn Văn Chiến on 1/8/21.
//

import UIKit

class CellGrid: UICollectionViewCell {

    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewColorLeft: UIView!
    @IBOutlet weak var viewBackground: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewBackground.layer.masksToBounds = true
        viewBackground.layer.cornerRadius = scale * 20
    }

}
